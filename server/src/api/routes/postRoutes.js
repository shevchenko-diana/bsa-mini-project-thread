import { Router } from 'express';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/id/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .put('/id/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => {
      if (post.userId !== req.user.id) {
        // todo extract into middleware
        throw new Error('You can\'t edit somebody\' post');
      }
      return postService.updatePost(req.params.id, req.body);
    })
    .then(post => {
      res.send(post);
    })
    .catch(next))
  .delete('/id/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => {
      if (post.userId !== req.user.id) {
        throw new Error('You can\'t edit somebody\' post');
      }
      return postService.deletePost(req.params.id);
    })
    .then(() => {
      res.send({ id: req.params.id });
    })
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.post && (reaction.post.userId !== req.user.id)) {
        // notify a user if someone (not himself) liked his post
        if (reaction.isLike) {
          req.io.to(reaction.post.userId)
            .emit('like', 'Your post was liked!');
        } else {
          req.io.to(reaction.post.userId)
            .emit('dislike', 'Your post was disliked!');
        }
      }
      return res.send(
        {
          id: reaction.id,
          isLike: reaction.isLike,
          createdAt: reaction.createdAt,
          updatedAt: reaction.updatedAt,
          userId: reaction.userId,
          postId: reaction.postId,
          updateOpposite: reaction.updateOpposite,
          post: reaction.post
        }
      );
    })
    .catch(next));

export default router;
