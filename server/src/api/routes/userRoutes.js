import { Router } from 'express';
import * as userService from '../services/userService';
import jwtMiddleware from '../middlewares/jwtMiddleware';

const router = Router();

router
  .put('/username/:userId', jwtMiddleware,
    (req, res, next) => userService.updateUsername(req.params.userId, req.body.username)
      .then(result => res.send(result))
      .catch(next));

export default router;
