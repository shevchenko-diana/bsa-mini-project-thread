import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/id/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/id/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => {
      if (comment.userId !== req.user.id) {
        // todo extract into middleware
        throw new Error('You can\'t edit somebody\' comment');
      }
      return commentService.updateCommentById(req.params.id, req.body);
    })
    .then(comment => {
      res.send(comment);
    })
    .catch(next))
  .delete('/id/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => {
      if (comment.userId !== req.user.id) {
        // todo extract into middleware
        throw new Error('You can\'t delete somebody\'s comment');
      }
      return comment;
    })
    .then(comment => {
      commentService.deleteComment(req.params.id, req.body);
      return comment;
    })
    .then(comment => {
      res.send({ postId: comment.postId });
    })
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    // todo SAME problem with lost field
    .then(reaction => res.send({
      id: reaction.id,
      body: reaction.body,
      status: reaction.status,
      createdAt: reaction.createdAt,
      updatedAt: reaction.updatedAt,
      userId: reaction.userId,
      postId: reaction.postId,
      commentCount: reaction.commentCount,
      likeCount: reaction.likeCount,
      dislikeCount: reaction.dislikeCount,
      user: reaction.user,
      commentReactions: reaction.commentReactions,
      updateOpposite: reaction.updateOpposite
    }))
    .catch(next));

export default router;
