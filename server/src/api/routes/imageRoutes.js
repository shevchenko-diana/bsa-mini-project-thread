import { Router } from 'express';
import * as imageService from '../services/imageService';
import * as userService from '../services/userService';
import imageMiddleware from '../middlewares/imageMiddleware';
import jwtMiddleware from '../middlewares/jwtMiddleware';

const router = Router();

router
  .post('/', imageMiddleware, (req, res, next) => imageService.upload(req.file)
    .then(image => res.send(image))
    .catch(next))
  .put('/:userId', jwtMiddleware,
    (req, res, next) => userService.updateUserImageById(req.params.userId, req.body.imageId)
      .then(id => res.send(id))
      .catch(next));

export default router;
