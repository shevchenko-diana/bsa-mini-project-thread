import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';
import { ACTIVE } from '../../data/db/statuses';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  status: ACTIVE,
  userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const updateCommentById = (id, body) => commentRepository.updateById(id, body);

export const deleteComment = id => commentRepository.deleteCommentById(id);

export const setReaction = async (userId, { commentId, isLike = true }) => {
  // define the callback for future use as a promise
  let updateOpposite = false;
  const update = id => {
    updateOpposite = true;
    return commentReactionRepository.updateById(id, { isLike });
  };
  const updateOrDelete = react => (react.isLike === isLike
    ? commentReactionRepository.deleteById(react.id)
    : update(react.id));

  const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);
  const result = reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepository.create({
      userId,
      commentId,
      isLike
    });
  const comment = await commentRepository.getCommentById(commentId);
  // todo fix lost field
  if (updateOpposite) {
    Object.assign(comment, { updateOpposite });
    comment.updateOpposite = updateOpposite;
  }
  return Number.isInteger(result) ? {} : comment;
};
