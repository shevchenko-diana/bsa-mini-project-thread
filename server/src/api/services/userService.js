import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, email, imageId, image } = await userRepository.getUserById(userId);
  return {
    id,
    username,
    email,
    imageId,
    image
  };
};

export const updateUserImageById = async (userId, imageId) => {
  const { id } = await userRepository.updateById(userId, { imageId });
  return { id };
};

export const updateUsername = async (userId, username) => {
  const res = await userRepository.updateById(userId, { username });
  return res;
};
