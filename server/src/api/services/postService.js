import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import { ACTIVE } from '../../data/db/statuses';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const updatePost = (id, body) => postRepository.updateById(id, body);

export const deletePost = id => postRepository.deletePostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  status: ACTIVE,
  userId
});

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  let updateOpposite = false;
  const update = id => {
    updateOpposite = true;
    return postReactionRepository.updateById(id, { isLike });
  };
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : update(react.id));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({
      userId,
      postId,
      isLike
    });
  // the result is an integer when an entity is deleted
  // return Number.isInteger(result) ? {} : result;
  const postReaction = await postReactionRepository.getPostReaction(userId, postId);
  if (updateOpposite) {
    postReaction.updateOpposite = true;
    Object.assign(postReaction, { updateOpposite: true });
  }
  return Number.isInteger(result) ? {} : postReaction;
};
