import sequelize from '../db/connection';
import {
  PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel, CommentReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';
import { DELETED } from '../db/statuses';

const Sequelize = require('sequelize');

const { Op } = Sequelize;
const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

// eslint-disable-next-line no-unused-vars
const commentlikeCase = bool => `CASE WHEN "comment.commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      notUserId
    } = filter;
    const where = {};
    Object.assign(where, {
      status: {
        [Op.not]: DELETED
      }
    });
    if (userId) {
      Object.assign(where, { userId });
    }
    if (notUserId) {
      Object.assign(where, {
        userId: {
          [Op.not]: notUserId
        }
      });
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          // TODO fix this workaround
          // query looks for active posts and counts their comments with the same status as posts - active
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."status" = "post"."status" 
                        )`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    const where = {};
    Object.assign(where, {
      status: {
        [Op.not]: DELETED
      },
      id
    });
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'comments->commentReactions.id',
        'comments->commentReactions.commentId',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."status" = "post"."status" 
                        )`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        required: false,
        where: {
          status: {
            [Op.not]: DELETED
          }
        },
        include: [
          {
            model: UserModel,
            attributes: ['id', 'username'],
            include: {
              model: ImageModel,
              attributes: ['id', 'link']
            }
          },
          {
            model: CommentReactionModel,
            required: false,
            attributes: ['commentId', 'userId', 'isLike']
          }
        ],
        // todo duplicate code
        attributes: ['id', 'body', 'status', 'userId', 'postId',
          [sequelize.literal(`
                       (Select count(*) from "comments" as "comment"
                        LEFT OUTER JOIN "commentReactions" AS "commentReactions"
                                 ON "comments"."id" = "commentReactions"."commentId"
                                  WHERE "comment"."id" = "commentReactions"."commentId" 
                                  and "commentReactions"."isLike" = false)`), 'dislikeCount'],
          [sequelize.literal(`
                       (Select count(*)  from "comments" as "comment"
                        LEFT OUTER JOIN "commentReactions" AS "commentReactions"
                                 ON "comments"."id" = "commentReactions"."commentId"
                                 WHERE "comment"."id" = "commentReactions"."commentId" 
                                 And "commentReactions"."isLike" = true)`), 'likeCount']
        ]
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }]
    });
  }

  deletePostById(id) {
    return this.model.update({ status: DELETED }, {
      where: { id },
      returning: true,
      plain: true
    });
  }
}

export default new PostRepository(PostModel);
