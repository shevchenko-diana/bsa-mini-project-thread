import { Op } from 'sequelize';
import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';
import { DELETED } from '../db/statuses';

import sequelize from '../db/connection';

const commentLikeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class CommentRepository extends BaseRepository {
  getCommentById(id) {
    const where = {};
    Object.assign(where, {
      status: {
        [Op.not]: DELETED
      },
      id
    });
    return this.model.findOne({
      group: [
        'comment.id',
        'commentReactions.id',
        'user.id',
        'user->image.id'
      ],
      where,
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: CommentReactionModel,
        required: false,
        attributes: ['commentId', 'userId', 'isLike']
      }],
      attributes: {
        include: [
          // [sequelize.literal(`
          //               (SELECT COUNT(*)
          //               FROM "commentReactions" as "commentReactions"
          //               WHERE "comment"."id" = "commentReactions"."commentId"
          //               )`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(commentLikeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(commentLikeCase(false))), 'dislikeCount']
        ]
      }
    });
  }

  deleteCommentById(id) {
    return this.model.update({ status: DELETED }, {
      where: { id },
      returning: true,
      plain: true
    });
  }
}

export default new CommentRepository(CommentModel);
