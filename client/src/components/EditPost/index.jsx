import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment, Modal } from 'semantic-ui-react';

import styles from './styles.module.scss';

const EditedPost = ({
  editedPost,
  updatePost,
  uploadImage,
  close
}) => {
  const [body, setBody] = useState(editedPost.body);
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);
  const [uploadingError, setUploadingError] = useState(undefined);

  const handleUpdatePost = async () => {
    if (!body) {
      return;
    }
    await updatePost({
      id: editedPost.id,
      request: {
        imageId: image?.imageId,
        body
      }
    });
    close();
  };

  const handleUploadFile = async ({ target }) => {
    setImage(undefined);
    setIsUploading(true);
    await uploadImage(target.files[0])
      .then(res => {
        setImage({
          imageId: res.id,
          imageLink: res.link
        });
        setUploadingError(undefined);
      })
      .catch(err => setUploadingError(err))
      .finally(() => setIsUploading(false));
  };

  // try {
  //   const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
  //
  // } catch (e) {
  //   setUploadingError(e);
  // } finally {
  //   // TODO: show error
  //   setIsUploading(false);
  // }

  return (
    <Segment>
      <Modal open onClose={close}>
        <Modal.Header className={styles.header}>
          <span>Update Post</span>
        </Modal.Header>
        <Modal.Content>
          <Form onSubmit={handleUpdatePost}>
            <Form.TextArea
              name="body"
              value={body}
              placeholder="What is the news?"
              onChange={ev => setBody(ev.target.value)}
            />
            {image?.imageLink && (
              <div className={styles.imageWrapper}>
                <Image className={styles.image} src={image?.imageLink} alt="post" />
              </div>
            )}
            <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
              <Icon name="image" />
              {/* todo show preview of an image */}
              {editedPost.image ? 'Change image' : 'Attach image'}
              <input name="image" type="file" onChange={handleUploadFile} hidden />
            </Button>
            <Button floated="right" color="blue" type="submit">Update</Button>
            {uploadingError && <span color="danger">Uploading error occurred. Try again or change image</span>}
          </Form>
        </Modal.Content>
      </Modal>

    </Segment>
  );
};

EditedPost.propTypes = {
  editedPost: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default EditedPost;
