import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Comment as CommentUI, CommentAction, CommentActions, Form, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from '../../helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({
  comment, userId, updateComment, deleteComment, likeComment,
  // eslint-disable-next-line react/prop-types
  dislikeComment, likeCount, dislikeCount
}) => {
  const [editComment, setEditComment] = useState(undefined);
  const [editCommentBody, setEditCommentBody] = useState(undefined);

  const handleEditComment = () => {
    updateComment(editComment.id, { body: editCommentBody })
      .finally(
        () => {
          setEditComment(undefined);
          setEditCommentBody(undefined);
        }
      );
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(comment.user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {comment.user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(comment.createdAt)
            .fromNow()}
        </CommentUI.Metadata>
        {!editComment
        && (
          <CommentUI.Text>
            {comment.body}
          </CommentUI.Text>
        )}

        {editComment && (
          <Form reply onSubmit={handleEditComment}>
            <Form.TextArea
              value={editCommentBody}
              placeholder="Type a comment..."
              onChange={ev => setEditCommentBody(ev.target.value)}
            />
            <Button type="submit" content="Update comment" labelPosition="left" icon="edit" primary />
            <Button
              type="button"
              content="Cancel edit"
              labelPosition="left"
              onClick={() => setEditComment(undefined)}
              icon="cancel"
            />
          </Form>
        )}
        <CommentActions>
          <CommentAction onClick={() => likeComment(comment.id)}>
            {' '}
            <Icon
              name="thumbs up"
            />
            {likeCount}
          </CommentAction>
          <CommentAction onClick={() => dislikeComment(comment.id)}>
            {' '}
            <Icon
              name="thumbs down"
            />
            {dislikeCount}
          </CommentAction>
          {(userId === comment.user.id && !editComment)
            ? (
              <>
                <CommentAction onClick={() => {
                  setEditComment(comment);
                  setEditCommentBody(comment.body);
                }}
                >
                  Edit
                </CommentAction>

                <CommentAction onClick={() => deleteComment(comment.id)}>Delete</CommentAction>
              </>
            ) : null}
        </CommentActions>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  updateComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired
};

export default Comment;
