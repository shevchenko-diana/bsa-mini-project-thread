import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';

const DeletePost = ({ postId, deletePost, close }) => (
  <Modal open onClose={close}>
    <Modal.Header className={styles.header}>
      <span>Are you sure? This action cannot be undone</span>
    </Modal.Header>
    <Modal.Content>
      <Button
        onClick={() => {
          deletePost(postId);
          close();
        }}
        color="red"
      >
        Delete post
      </Button>
    </Modal.Content>
  </Modal>
);

DeletePost.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

export default DeletePost;
