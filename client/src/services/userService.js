/* eslint-disable */
import callWebApi from '../helpers/webApiHelper';

export const updateUsername = async (username, userId) => {
  const response = await callWebApi({
    endpoint: `/api/users/username/${userId}`,
    type: 'PUT',
    request: {
      username
    }
  });
  return response.json();
};
