import callWebApi from '../helpers/webApiHelper';

export const uploadImage = async image => {
  const response = await callWebApi({
    endpoint: '/api/images',
    type: 'POST',
    attachment: image
  });
  return response.json();
};

export const updateImage = async (imageId, userId) => {
  const response = await callWebApi({
    endpoint: `/api/images/${userId}`,
    type: 'PUT',
    request: { imageId }
  });
  return response.json();
};
