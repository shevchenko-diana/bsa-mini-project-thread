import React, { useState } from 'react';
import ReactCrop from 'react-image-crop';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Grid, Icon, Image, Input, Modal, ModalActions } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import { getUserImgLink } from '../../helpers/imageHelper';
import 'react-image-crop/dist/ReactCrop.css';
import styles from './styles.module.scss';
import * as imageService from '../../services/imageService';
import { updateUserImage, updateUserName } from './actions';

// todo divide into separate components of Image Update and Username Update

const Profile = ({ user, updateUserImage: updateImg, updateUserName: updateName }) => {
  const [imageRef, setImageRef] = useState(undefined);
  const [rawSrc, setRawSrc] = useState(undefined);
  const [imageModalOpen, setImageModalOpen] = useState(false);
  const [uploadingError, setUploadingError] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);

  const [usernameModalOpen, setUsernameModalOpen] = useState(false);
  const [username, setUsername] = useState(false);
  const [usernameErr, setUsernameErr] = useState(undefined);
  const uploadImage = file => imageService.uploadImage(file);

  const [crop, setCrop] = useState({
    unit: 'px',
    width: 100,
    aspect: 1
  });

  const onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => setRawSrc(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
    setImageModalOpen(true);
  };

  const uploadCropped = async file => {
    setIsUploading(true);
    return uploadImage(file)
      .then(res => {
        setUploadingError(undefined);
        return res;
      })
      .catch(err => setUploadingError(err))
      .finally(() => {
        setIsUploading(false);
      });
  };
  const cropAndUpload = async (image, fileName) => {
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );
    return new Promise(resolve => {
      canvas.toBlob(async blob => {
        if (!blob) {
          // reject(new Error('Canvas is empty'));
          console.error('Canvas is empty');
          return;
        }
        // eslint-disable-next-line no-param-reassign
        blob.name = fileName;
        setIsUploading(true);

        const res = await uploadCropped(blob);
        resolve(res);
      }, 'image/jpeg');
    });
  };
  const onImageUpdateCancel = () => {
    // todo make sure all is cleared
    setImageModalOpen(false);
    setRawSrc(undefined);
  };

  const onImageSave = async () => {
    const res = await cropAndUpload(imageRef, 'newFile.jpeg');
    updateImg(res, user.id);
    setImageModalOpen(false);
  };

  const onUsernameUpdate = async newUsername => {
    const { updateErr } = await updateName(newUsername, user.id);
    console.log(updateErr);
    if (updateErr !== '') {
      if (updateErr === 'Validation error') {
        setUsernameErr('This username is already used');
      } else {
        setUsernameErr(updateErr);
      }
    } else {
      setUsernameErr(false);
      setUsernameModalOpen(false);
    }
  };

  return (
    <Grid container textAlign="center" style={styles}>
      <Grid.Column>
        <Image centered src={getUserImgLink(user.image)} size="medium" circular />
        <br />
        <Button color="teal" icon labelPosition="left" as="label">
          <Icon name="image" />
          Change a picture
          <input type="file" accept="image/*" onChange={onSelectFile} hidden />
        </Button>

        <br />
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          disabled
          value={user.username}
        />
        <Button
          type="button"
          icon="edit"
          onClick={() => setUsernameModalOpen(true)}
        />
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        {usernameModalOpen
        && (
          <Modal open={usernameModalOpen}>
            <Modal.Header>
              <span>Update a username</span>
            </Modal.Header>
            <Modal.Content>
              <Input
                placeholder={user.username}
                type="text"
                onChange={ev => setUsername(ev.target.value)}
              />
              {usernameErr
              && (
                <>
                  <br />
                  <br />
                  <span style={{ color: 'red' }}>
                    {' '}
                    {usernameErr}
                  </span>
                </>
              )}
            </Modal.Content>
            <ModalActions>
              <Button
                type="button"
                content="Update username"
                labelPosition="left"
                onClick={() => onUsernameUpdate(username)}
                icon="image"
                color="green"
              />
              <Button
                type="button"
                content="Cancel"
                labelPosition="left"
                onClick={() => {
                  setUsernameModalOpen(false);
                }}
                icon="cancel"
                color="red"
              />
            </ModalActions>
          </Modal>
        )}
        {imageModalOpen
        && (
          <Modal open={imageModalOpen}>
            <Modal.Header>
              <span>Select and Crop image</span>
              {uploadingError
              && (
                <>
                  <br />
                  <br />
                  <span style={{ color: 'red' }}>Uploading error occurred. Try again or change image</span>
                </>
              )}

            </Modal.Header>
            <ModalActions>
              <Button
                type="button"
                content="Save selected area"
                labelPosition="left"
                onClick={onImageSave}
                icon="image"
                color="green"
                loading={isUploading}
              />
              <Button
                type="button"
                content="Cancel"
                labelPosition="left"
                onClick={onImageUpdateCancel}
                icon="cancel"
                color="red"
              />
            </ModalActions>
            <Modal.Content>
              <div />
              {rawSrc && (
                <ReactCrop
                  src={rawSrc}
                  crop={crop}
                  maxWidth={200}
                  maxHeight={200}
                  minWidth={50}
                  keepSelection
                  minHeight={50}
                  onImageLoaded={ref => setImageRef(ref)}
                  onChange={newCrop => setCrop(newCrop)}
                />
              )}

            </Modal.Content>
          </Modal>
        )}
      </Grid.Column>
    </Grid>
  );
};
Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateUserImage: PropTypes.func.isRequired,
  updateUserName: PropTypes.func.isRequired

};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = {
  updateUserImage,
  updateUserName
};
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
