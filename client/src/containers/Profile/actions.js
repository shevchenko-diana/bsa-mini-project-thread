import * as authService from '../../services/authService';
import * as imageService from '../../services/imageService';
import * as userService from '../../services/userService';
import { SET_USER } from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};

export const updateUserImage = (image, id) => async (dispatch, getRootState) => {
  await imageService.updateImage(image.id, id);
  const { profile: { user } } = getRootState();

  const newUser = {
    ...user,
    imageId: image.id,
    image
  };
  dispatch(setUser(newUser));
};

export const updateUserName = (username, id) => async (dispatch, getRootState) => {
  let updateErr = '';
  await userService.updateUsername(username, id)
    .then(() => {
      updateErr = '';
    })
    .catch(
      err => {
        updateErr = err.message;
      }
    );
  if (updateErr) {
    return { updateErr };
  }
  const { profile: { user } } = getRootState();

  const newUser = {
    ...user,
    username
  };
  dispatch(setUser(newUser));
  return {
    updateErr
  };
};
