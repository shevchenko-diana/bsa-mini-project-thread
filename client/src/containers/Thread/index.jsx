/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import * as imageService from '../../services/imageService';
import {
  addPost,
  deletePost,
  dislikePost,
  likePost,
  loadMorePosts,
  loadPosts,
  toggleExpandedPost,
  updatePost
} from './actions';
import ExpandedPost from '../ExpandedPost';
import Post from '../../components/Post';
import AddPost from '../../components/AddPost';
import DeletePost from '../../components/DeletePost';
import SharedPostLink from '../../components/SharedPostLink';

import styles from './styles.module.scss';
import EditedPost from '../../components/EditPost';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost,
  updatePost: editPost,
  deletePost: removePost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [editedPost, setEditedPost] = useState(undefined);
  const [deletedPostId, setDeletedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [hideOwnPosts, setHideOwnPosts] = useState(false);

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    if (!showOwnPosts) {
      setHideOwnPosts(false);
    }
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    postsFilter.notUserId = undefined;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };
  const toggleHideOwnPosts = () => {
    setHideOwnPosts(!hideOwnPosts);
    if (!hideOwnPosts) {
      setShowOwnPosts(false);
    }
    postsFilter.notUserId = hideOwnPosts ? undefined : userId;
    postsFilter.userId = undefined;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          toggle
          label="Hide my posts"
          checked={hideOwnPosts}
          onChange={toggleHideOwnPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            ownPost={post.userId === userId}
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
            updatePost={() => setEditedPost(post)}
            deletePost={() => setDeletedPostId(post.id)}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          setDeletedPostId={setDeletedPostId}
          setEditedPost={setEditedPost}
          userId={userId}
          sharePost={sharePost}
        />
      )}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
      {deletedPostId
      && <DeletePost postId={deletedPostId} close={() => setDeletedPostId(undefined)} deletePost={removePost} />}
      {editedPost && (
        <EditedPost
          editedPost={editedPost}
          updatePost={editPost}
          uploadImage={uploadImage}
          close={() => setEditedPost(undefined)}
        />
      )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  deletePost,
  updatePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
