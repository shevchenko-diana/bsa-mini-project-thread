import * as postService from '../../services/postService';
import * as commentService from '../../services/commentService';
import { ADD_POST, LOAD_MORE_POSTS, SET_ALL_POSTS, SET_EXPANDED_POST } from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const updatePost = ({ id, request }) => async (dispatch, getRootState) => {
  await postService.updatePost(id, request);
  const newPost = await postService.getPost(id);
  const { posts: { posts } } = getRootState();
  const updated = posts.map(post => (post.id !== newPost.id
    ? post : newPost));

  dispatch(setPostsAction(updated));
};
export const deletePost = id => async (dispatch, getRootState) => {
  await postService.deletePost(id);
  const { posts: { posts } } = getRootState();
  const updated = posts.filter(post => post.id !== id);
  dispatch(setPostsAction(updated));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { id, updateOpposite } = await postService.likePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const mapReaction = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff,
    dislikeCount: Number(post.dislikeCount) + (updateOpposite ? -1 : 0)
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapReaction(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapReaction(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { id, updateOpposite } = await postService.dislikePost(postId);
  const diff = id ? 1 : -1;

  const mapReaction = post => ({
    ...post,
    likeCount: Number(post.likeCount) + (updateOpposite ? -1 : 0),
    dislikeCount: Number(post.dislikeCount) + diff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapReaction(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapReaction(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const updateComment = (id, request) => async (dispatch, getRootState) => {
  const comment = await commentService.updateComment(id, request);
  const updatedPost = await postService.getPost(comment.postId);
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== updatedPost.id
    ? post : updatedPost));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const deleteComment = id => async (dispatch, getRootState) => {
  const { postId } = await commentService.deleteComment(id);
  const updatedPost = await postService.getPost(postId);
  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== updatedPost.id
    ? post : updatedPost));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const { id, updateOpposite } = await commentService.likeComment(commentId);
  const diff = id ? 1 : -1;// if ID exists then the post was liked, otherwise - like was removed
  const mapCommentsReaction = comments => (comments.map(comment => (comment.id === commentId
    ? {
      ...comment,
      likeCount: Number(comment.likeCount) + diff,
      dislikeCount: Number(comment.dislikeCount) + (updateOpposite ? -1 : 0)
    } : comment)));
  const { posts: { expandedPost } } = getRootState();

  dispatch(setExpandedPostAction({
    ...expandedPost,
    comments: mapCommentsReaction(expandedPost.comments)
  }));
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const { id, updateOpposite } = await commentService.dislikeComment(commentId);
  const diff = id ? 1 : -1;
  const mapCommentsReaction = comments => (comments.map(comment => (comment.id === commentId
    ? {
      ...comment,
      likeCount: Number(comment.likeCount) + (updateOpposite ? -1 : 0),
      dislikeCount: Number(comment.dislikeCount) + diff
    } : comment)));
  const { posts: { expandedPost } } = getRootState();

  dispatch(setExpandedPostAction({
    ...expandedPost,
    comments: mapCommentsReaction(expandedPost.comments)
  }));
};
