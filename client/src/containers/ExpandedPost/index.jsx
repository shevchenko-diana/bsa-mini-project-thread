import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Comment as CommentUI, Header, Modal } from 'semantic-ui-react';
import moment from 'moment';
import {
  addComment,
  deleteComment,
  dislikeComment,
  dislikePost,
  likeComment,
  likePost,
  toggleExpandedPost,
  updateComment
} from '../Thread/actions';
import Post from '../../components/Post';
import Comment from '../../components/Comment';
import AddComment from '../../components/AddComment';
import Spinner from '../../components/Spinner';

const ExpandedPost = ({
  userId,
  post,
  sharePost,
  setEditedPost,
  setDeletedPostId,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  addComment: add,
  likeComment: likeCom,
  dislikeComment: dislikeCom,
  deleteComment: deleteCom,
  updateComment: update
}) => (

  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Post
            ownPost={post.userId === userId}
            updatePost={() => {
              setEditedPost(post);
              toggle();
            }}
            deletePost={() => {
              setDeletedPostId(post.id);
              toggle();
            }}
            key={post.id}

            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            sharePost={sharePost}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt)
                .diff(c2.createdAt))
              .map(comment => (
                <Comment
                  key={comment.id}
                  userId={userId}
                  comment={comment}
                  updateComment={update}
                  deleteComment={deleteCom}
                  likeCount={comment.likeCount}
                  dislikeCount={comment.dislikeCount}
                  likeComment={likeCom}
                  dislikeComment={dislikeCom}
                />
              ))}
            <AddComment postId={post.id} addComment={add} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);
ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  setDeletedPostId: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  setEditedPost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost,
  likeComment,
  dislikeComment,
  addComment,
  deleteComment,
  updateComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
